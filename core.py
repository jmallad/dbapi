#!/usr/bin/env python

import os, psycopg2, uuid, psycopg2.extras, config
from werkzeug.utils import secure_filename
psycopg2.extras.register_uuid()

def gen_uuid(path):
    ret = uuid.uuid4()
    if os.path.exists(os.path.join(path, str(ret))):
        return gen_uuid(path)
    return ret

def file_path(path, public):
    if public == "true":
        path = "/www/assets/{}".format(secure_filename(path))
    else:
        path = "/www/assets/private/{}".format(secure_filename(path))    
    return path

class dbapi():
    def __init__(self):
        self.dbcur = None
        self.dbconn = None
    def connect(self, host, port, dbname, user, password):
        self.dbconn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
        self.dbcur = self.dbconn.cursor()
    def commit(self):
        self.dbconn.commit()
    def close(self):
        self.dbconn.close()
    def execute(self, query):
        self.dbcur.execute(query)
    def select(self, query):
        self.dbcur.execute(query)
        return self.dbcur.fetchall()
