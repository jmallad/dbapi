This is a lightweight utility which tunnels SQL queries over HTTP. I basically
made this to put my Postgres instance behind Cloudflare. 