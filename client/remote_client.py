
#!/usr/bin/env python3

import sys, os, urllib, pickle, aiohttp, asyncio, json, nest_asyncio
import logging, pickle
from systemd.journal import JournalHandler

nest_asyncio.apply()

class objview(object):
    def __init__(self, d):
        self.__dict__ = d

class apiclient_v3():
    def __init__(self, server, key):
        self.server = server
        self.key = key
        self.loop = asyncio.get_event_loop()
        self.log = logging.getLogger("sqlapi")
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.INFO)        
        
    async def request(self, endpoint, data):
        data["key"] = self.key
        async with aiohttp.ClientSession() as session:
            async with session.post("{}/v3/{}".format(self.server, endpoint),
                                    data=data) as resp:
                response = await resp.read()
                ret = response.decode('utf-8')
                self.log.info(ret)
                self.log.info(type(ret))
                if ret:
                    return json.loads(ret)
                else:
                    return (500, None)

    def call(self, endpoint, data={}):
        return self.loop.run_until_complete(self.request(endpoint, data))

    def select(self, query):
        return self.call("select", {"query":query})

    def execute(self, query):
        return self.call("execute", {"query":query})


    
